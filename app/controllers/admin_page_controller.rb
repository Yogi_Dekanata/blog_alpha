class AdminPageController < ApplicationController

  layout 'admin'
  before_action :logged_in_user, only: [:dashboard]

  def dashboard
  end

  def new

  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
# Log the user in and redirect to the user's show page.
      log_in user
      redirect_to dashboard_path

    else
# Create an error message.
      flash.now[:danger] = 'Invalid email/password combination'

      render 'new'
    end
  end

  def destroy
    log_out
    reset_session

    redirect_to root_url
  end

  def logged_in_user
    unless logged_in?
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

end
